import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
import numpy as np
import csv

class ReportGeneration:
    def ParseCSVfile(fpath):
        populatedTable = []
        header = []
        stringColumn = []
        with open(fpath, 'r') as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=';')
            for i, row in enumerate(csv_reader):
                if (i == 0):
                    header.append(row)
                if row[0].startswith('Visit'):
                    stringColumn.append(row[0])
                    populatedTable.append(row[1:])

        for i in populatedTable:
            for j, s in enumerate(i):
                if (s):
                    i[j] = float(s.replace(',','.'))
                else: i[j] = None

        header = [item for sublist in header for item in sublist]
        df = pd.DataFrame(populatedTable, columns=header[1:])
        df[header[0]] = stringColumn
        return df

    def Visualize(dframe):
        labels = list(dframe.loc[:, dframe.dtypes == object])
        newKK = dframe.transpose().reset_index()
        cNames = list(dframe['Event Name'])
        cNames.insert(0, 'Measures')
        newKK.columns = cNames
        res = newKK.iloc[:6, :]
        with PdfPages(r'Output/FinalReport.pdf') as pdf:
            column_headers = list(res.columns)[1:]
            cell_text = []
            k = 0
            for i in range(1, len(res.index) + 1):
                cell_text.append(list(res.iloc[k:i, 1:].values[0]))
                k += 1
            row_headers = list(res[res.columns[0]])
            figz, ax1 = plt.subplots(figsize=(8, 3))
            rcolors = np.full(len(row_headers), 'linen')
            ccolors = np.full(len(column_headers), 'lavender')
            table = ax1.table(cellText=cell_text,
                              cellLoc='center',
                              rowLabels=row_headers,
                              rowColours=rcolors,
                              rowLoc='center',
                              colColours=ccolors,
                              colLabels=column_headers,
                              loc='center')
            table.scale(1, 2)
            table.set_fontsize(16)
            ax1.axis('off')
            title = "Certificate of Participation"
            subtitle = "Xxx name xxx has been in the study since xxx date xxx"
            ax1.set_title(f'{title}\n({subtitle})', weight='bold', size=14, color='k')
            plt.rcParams['figure.dpi'] = 200
            plt.rcParams['savefig.bbox'] = 'tight'
            pdf.savefig(figz)

            fig, axes = plt.subplots(nrows=1, ncols=2, figsize=(8, 3))
            dframe.plot.bar(ax=axes[0], x=labels[0], y=['Right Hand Average', 'Left Hand Average'], rot=0)
            dframe.plot.bar(ax=axes[1], x=labels[0], y=['MDT-PD simple sum score', 'FAQ Score'], rot=0)
            fig.tight_layout()
            pdf.savefig(fig)

            fig2, axes2 = plt.subplots(nrows=1, ncols=1, figsize=(8, 3))
            dframe.plot(ax=axes2, kind='bar', x=labels[0], y='Total Score Part III:')
            dframe.plot(ax=axes2, x=labels[0], y='Total Points for MOCA:')
            pdf.savefig(fig2)